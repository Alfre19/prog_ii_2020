class EmployeeNum(dict):
    def __init__(self, f):
        super().__init__()
        self.f = f

    def __missing__(self, key):
        self[key] = self.f()
        return self[key]


d = EmployeeNum(list)
d['pepe']