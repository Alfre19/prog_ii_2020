"""
Example for execution stack

Run debug mode with breakpoints in outer and inner calculations.

"""
from typing import Final

MY_CONSTANT: Final[int] = 100


def outer(x: int) -> int:
    outer_result = inner(x) + 1
    return outer_result


def inner(y: int) -> int:
    inner_result = y ** 2
    return inner_result


if __name__ == '__main__':
    computation = outer(MY_CONSTANT)
    print(computation)
