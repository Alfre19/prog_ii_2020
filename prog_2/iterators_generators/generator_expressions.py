

it = (i*i for i in range(10))
sum(it)

sum(i*i for i in range(10))                 # sum of squares


xvec = [10, 20, 30]
yvec = [7, 5, 3]
sum(x*y for x,y in zip(xvec, yvec))         # dot product

with open('prog_2/iterators_generators/sample_page.txt') as page:
    unique_words = set(word.lower() for line in page for word in line.split())

with open('prog_2/iterators_generators/sample_page.txt') as page:
    for line in page:
        print(line)

with open('prog_2/iterators_generators/sample_page.txt') as line:
    print(line)

from dataclasses import dataclass


@dataclass
class Graduate:
    name: str
    gpa: float


graduates = [Graduate('john', 4.95), Graduate('mary', 5.0), Graduate('lucy', 4.0)]
valedictorian = max((student.gpa, student.name) for student in graduates)

data = 'golf'
list(data[i] for i in range(len(data)-1, -1, -1))


