from dataclasses import dataclass
import datetime as dt
import uuid


@dataclass
class Transaction:
    """A transaction"""
    transaction_number: uuid.UUID
    type: str
    date: dt.datetime
    quantity: float

    @classmethod
    def new_invoice(cls, quantity: float) -> 'Transaction':
        """
        Creates a new transction object representing an invoice.
        :param quantity: the total amount of the invoice
        """
        if not isinstance(quantity, float):
            raise ValueError

        return cls(transaction_number=uuid.uuid4(),
                   type='invoice',
                   date= dt.datetime.now(),
                   quantity=quantity)


v1 = Transaction.new_invoice(10)


