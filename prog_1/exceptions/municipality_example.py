import re


class WrongPostalCode(Exception):
    def __init__(self, postal_code):
        self.postal_code = postal_code


def show_municipality(address: str):
    m = re.search(r'(\d{2})\d{3} (.+)', address)
    if m.group(1) != '28':
        raise WrongPostalCode(m.group(1))

    return m.group(2)


try:
    a = show_municipality('Camino Alto 83, 27109 Alcobendas')
except WrongPostalCode as error:
    print(f'Postal code does not start with 28, starts with {error.postal_code}')
else:
    print(f'Municipality: {a}')