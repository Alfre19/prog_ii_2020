import concurrent.futures
import time

start = time.perf_counter()


def do_something(seconds):
    print(f'Sleeping {seconds} second(s)...')
    time.sleep(seconds)
    return f'Done Sleeping...{seconds}'


if __name__ == '__main__':
    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        secs = [5, 4, 3, 2, 1]
        # Similar to map(func, *iterables) except:
        # - the iterables are collected immediately rather than lazily;
        # - func is executed asynchronously and several calls to func are made concurrently.
        #
        # This method chops iterables into a number of chunks which it submits
        # to the pool as separate tasks. The (approximate) size of these chunks can be specified
        # by setting chunksize to a positive integer.

        results = executor.map(do_something, secs, chunksize=1)
        # Until all processes finish
        for result in results:
            print(result)

    finish = time.perf_counter()

    print(f'Finished in {round(finish - start, 2)} second(s)')