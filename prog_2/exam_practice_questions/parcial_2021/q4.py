from dataclasses import dataclass
import datetime as dt


@dataclass
class Transaction:
    id: str
    date: dt.date
    items: list[float]

    def __lt__(self, other: 'Transaction') -> bool:
        return sum(self.items) < sum(other.items)


records = [
    Transaction('01', dt.date(2021,1,2), [1.0, 2.0, 3.0]),
    Transaction('02', dt.date(2021, 1, 4), [5.0, 2.0, 3.0]),
    Transaction('03', dt.date(2021, 1, 9), [10.0, 2.0, 3.0]),
]

sorted(records, key=lambda t: sum(t.items), reverse=True)
sorted(records)
sorted(records, key=lambda t: t.date, reverse=True)


@dataclass
class TransactionD:
    id: str
    items: dict[str: float]

    def __lt__(self, other: 'TransactionD') -> bool:
        return sum(self.items.values()) < sum(other.items.values())


records = [
    TransactionD('01', {'A': 1.0, 'B': 2.0, 'C': 3.0}),
    TransactionD('02', {'A': 10.0, 'B': 2.0, 'C': 3.0}),
    TransactionD('03', {'A': 5.0, 'B': 2.0, 'C': 3.0}),
]

sorted(records, key=lambda t: sum(t.items.values()), reverse=True)
sorted(records, reverse=True)