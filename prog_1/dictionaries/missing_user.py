from dataclasses import dataclass
from collections import UserDict


@dataclass
class User:
    name: str
    age: int


class Users(UserDict):
    def __missing__(self, name: str):
        age = int(input('Missing user, enter age: '))
        user = User(name, age)
        self[name] = user
        return user


users = Users()

name = input('Name?')
while name:
    print(f"The user is {users[name].age} years old")
    name = input('Name?')


