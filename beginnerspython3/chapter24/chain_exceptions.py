class DivideByYWhenZeroException(Exception):
    """ Sample Exception class"""


def divide(x, y):
    try:
        result = x / y
    except Exception as e:
        raise DivideByYWhenZeroException


divide(6, 0)

