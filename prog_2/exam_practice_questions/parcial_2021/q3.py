from collections import defaultdict


def arrange_stock(*items):
    for item in items:
        d[item[0]].add(item)


d = defaultdict(set)
arrange_stock("apple", "banana", "cherry", "strawberry",
              "raspberry", "almond", "avocado", "barberry")