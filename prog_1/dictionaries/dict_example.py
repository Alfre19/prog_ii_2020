import names
from dataclasses import dataclass
import random


@dataclass
class User:
    name: str
    age: int


name_list = [names.get_full_name() for i in range(10)]
d2 = {name: User(name, random.randint(0, 99)) for name in name_list}

del d2['James Grant']

d2['Tracy Brown'].age = 91

n = names.get_full_name()
d2.update({n: User(n, 18)})

d2['Tracy B']
d2.get('Tracy B', 0)

[user.split(' ')[0] for user in d2.keys()]

for name, user in d2.items():
    print(f'{name} is {user.age} years old')