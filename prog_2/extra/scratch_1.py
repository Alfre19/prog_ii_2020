from typing import Callable
from math import log, sin, cos

def log_transform_n(n: int) -> Callable:

    def log_transform(s: Callable) -> Callable:
        def t(x: int) -> float:
            return s(log(x * n))

        return t

    return log_transform


@log_transform_n(3)
def sin_cos_3(x):
    return sin(x) * cos(x)

# sin_cos_3 es esta función sin(log(x * 3)) + cos(log(x * 3))

@log_transform_n(4)
def sin_cos_4(x):
    return sin(x) * cos(x)

# sin_cos_4 es esta función sin(log(x * 4)) + cos(log(x * 4))
