selection = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97}


def selected_addition(*numbers: int) -> int:
    i = 0
    for n in numbers:
        if n in selection:
            i += n
    return i


selected_addition(1, 2, 3, 4, 5, 6)


def select_primes(*numbers: int) -> list:
    l = []
    for n in numbers:
        if n in selection:
            l.append(n)
    return l

select_primes(1, 2, 3, 4, 5, 6)