class RouteStep:
    """A step in a vehicle route at a given address"""
    def __init__(self, address: str):
        self.address = address


class Route:
    """A route with multiple route steps"""
    route_steps_num = 0

    def __init__(self, vehicle_name: str):
        self.vehicle_name = vehicle_name
        self.steps: list[RouteStep] = []

    def add_step(self, step: RouteStep):
        """Adds a step to the route. Updates the total number of steps added
        to instances of the class"""
        Route.route_steps_num += 1
        self.steps.append(step)

    def print_steps(self):
        """Prints the steps of the route"""
        print(', '.join([s.address for s in self.steps]))


# derived class
r = Route('LHY3333')
s1 = RouteStep('Vereda de Palacio 1')
s2 = RouteStep('Vereda de Palacio 2')
r.add_step(s1)
r.add_step(s2)

r.print_steps()

r2 = Route('LHY3444')
s1 = RouteStep('Vereda de Palacio 5')
s2 = RouteStep('Vereda de Palacio 6')
r2.add_step(s1)
r2.add_step(s2)

Route.route_steps_num