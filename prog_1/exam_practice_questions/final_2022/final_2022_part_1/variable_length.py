from collections import defaultdict


def split(*numbers) -> dict:
    d = defaultdict(list)
    for label, number in numbers:
        d[label].append(number)
    return d


print(split(('a', 1), ('b', 2), ('a', 3), ('a', 4)))
# {'a': [1, 3, 4], 'b': [2]})
