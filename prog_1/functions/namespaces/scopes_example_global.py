A_CONSTANT = 3

def outer():
    def inner():
        print(A_CONSTANT)

    another_variable = 2
    print(another_variable)
    inner()


outer()
