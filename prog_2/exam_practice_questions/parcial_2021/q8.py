from collections import deque


class CumMean:
    def __init__(self, values: deque[float]):
        self.values = values
        self.accum = 0
        self.n = 0

    def __iter__(self):
        return self

    def __next__(self):
        if not self.values:
            raise StopIteration
        self.accum += self.values.popleft()
        self.n += 1
        return self.accum / self.n


for i in CumMean(deque([2, 4, 6, 8])):
    print(i)


class CumMeanBack:
    def __init__(self, values: list[float]):
        self.values = values
        self.accum = 0
        self.n = 0

    def __iter__(self):
        return self

    def __next__(self):
        if not self.values:
            raise StopIteration
        self.accum += self.values.pop()
        self.n += 1
        return self.accum / self.n


for i in CumMeanBack([8, 6, 4, 2]):
    print(i)


class CumSubstract:
    def __init__(self, init: float, values: list[float]):
        values.reverse()
        self.values = values
        self.accum = init

    def __iter__(self):
        return self

    def __next__(self):
        if not self.values:
            raise StopIteration
        self.accum -= self.values.pop()
        return self.accum


for i in CumSubstract(20, [8, 6, 4, 2]):
    print(i)