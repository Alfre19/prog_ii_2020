#!/usr/bin/env python
# coding: utf-8

# # Building RNN, LSTM, and GRU for time series using PyTorch
# 
# From :
# https://towardsdatascience.com/building-rnn-lstm-and-gru-for-time-series-using-pytorch-a46e5b094e7b
# Dataset:
# https://www.kaggle.com/robikscube/hourly-energy-consumption
# More exampoles:
# https://www.crosstab.io/articles/time-series-pytorch-lstm

import torch
import torch.nn as nn
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, StandardScaler, MaxAbsScaler, RobustScaler
import holidays
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.linear_model import LinearRegression

import torch.optim as optim
from torch.utils.data import TensorDataset, DataLoader

import plotly.graph_objs as go
from plotly.offline import iplot

device = (torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu'))
print(f"Training on device {device}.")

# ## Loading the dataset

# Public dataset is [PJM's Hourly Energy Consumption data:
# (https://www.kaggle.com/robikscube/hourly-energy-consumption),
# a univariate time-series dataset of 10+ years of hourly observations collected from different
# US regions. I'll be using the PJM East region data, which originally has the hourly energy
# consumption data from 2001 to 2018


FILE_NAME = 'PJME_hourly.csv'

# Load dataset

df = pd.read_csv(FILE_NAME)

# Convert date time to the index. Power consumption label to vaule

df = df.set_index(['Datetime'])
df = df.rename(columns={'PJME_MW': 'value'})

df.index = pd.to_datetime(df.index)
if not df.index.is_monotonic:
    df = df.sort_index()


# Descrioptive analytics using


def plot_dataset(df, title):
    data = []

    value = go.Scatter(
        x=df.index,
        y=df.value,
        mode="lines",
        name="values",
        marker=dict(),
        text=df.index,
        line=dict(color="rgba(0,0,0, 0.3)"),
    )
    data.append(value)

    layout = dict(
        title=title,
        xaxis=dict(title="Date", ticklen=5, zeroline=False),
        yaxis=dict(title="Value", ticklen=5, zeroline=False),
    )

    fig = dict(data=data, layout=layout)
    iplot(fig)


plot_dataset(df, title='PJM East (PJME) Region: estimated energy consumption in Megawatts (MW)')

# The next step is to generate feature columns to transform our univariate dataset into a
# multivariate dataset. We will convert this time series into a supervised learning problem if
# you will. In some datasets, such features as hourly temperature, humidity, or precipitation,
# are readily available. However, in our dataset, no extra information could help us predict the
# energy consumption is given. So, it falls to our lot to create such predictors, i.e.,
# feature columns.


df_features = (
    df.assign(hour=df.index.hour,
              day=df.index.day,
              month=df.index.month,
              day_of_week=df.index.dayofweek,
              week_of_year=df.index.isocalendar().week)
)


# Although passing date and time features to the model without any touch may work in practice,
# it would certainly make it harder for the model to learn interdependencies between these
# features. For us humans, it is rather straightforward to see the hours, days, weeks and months
# follow somewhat cyclical patterns. While it is trivial for us to say that December is followed
# by January, it may not be very clear for algorithms to discern the first month of the year
# comes after the 12th one. One can easily come up with many more examples for that matter. So,
# this makes good feature engineering crucial for building deep learning models, even more so for
# traditional machine learning models.

# ### One-hot encoding
# 
# One way to encode DateTime features is to treat them as categorical variables and add a new
# binary variable for each unique value, widely known as one-hot encoding. Suppose you applied
# one-hot encoding on your month column, which ranges from 1 to 12. In this case, 12 new month
# columns are created, say [Jan, Feb,...… Dec] and only one of such columns has the value 1 while
# the rest being zeroed. For instance, some DateTime value from February should have the second
# of these encoded columns as 1, as in [0, 1,...… 0]. Using Pandas' get_dummies method we can
# easily create one-hot encoded columns from a given dataset.

def onehot_encode_pd(df, cols):
    dummies = [df.copy()]
    dummies.extend([pd.get_dummies(df[col], prefix=col) for col in cols])

    return pd.concat(dummies, axis=1).drop(columns=cols)


df_features = onehot_encode_pd(df_features, ['month', 'day', 'day_of_week', 'week_of_year'])
print(df_features)

# Though quite useful to encode categorical features, one-hot encoding does not fully capture the
# cyclical patterns in DateTime features. It simply creates categorical buckets, if you will,
# and lets the model learn from these seemingly independent features. Encoding the day of the
# week in a similar manner, for instance, loses the information that Monday is closer to Tuesday
...


# For some use cases, this may not matter too much, indeed. In fact, with enough data, training
# time, and model complexity, the model may learn such relationships between such features
# independently. But there is also another way.

# ### Generating cyclical features (sin/cos transformation)
# 
# As with all the data we have worked on until now, some data is inherently cyclical. Be it
# hours, days, weeks, or months, they all follow periodic cycles. Again, this is trivial for us
# to see, but not so much for machine learning models. The problem simply becomes how can we tell
# algorithms that the hours 23 and 0 are as close as hour 1 is to hour 2?
# 
# The gist is to create two new cyclical features, calculating sine and cosine transform of the
# given DateTime feature, say the hour of the day. Instead of using the original value for the
# hour, the model then uses the sine transform of the hour, preserving the cyclicality of the
# model. To see how and why it works, feel free to have a look at [Pierre-Louis'](
# https://towardsdatascience.com/cyclical-features-encoding-its-about-time-ce23581845ca) or [
# David's blog](http://blog.davidkaleko.com/feature-engineering-cyclical-features.html) post on
# the matter, which explains the concept more in detail.


def generate_cyclical_features(df, col_name, period, start_num=0):
    kwargs = {
        f'sin_{col_name}': lambda x: np.sin(2 * np.pi * (df[col_name] - start_num) / period),
        f'cos_{col_name}': lambda x: np.cos(2 * np.pi * (df[col_name] - start_num) / period)
    }
    return df.assign(**kwargs).drop(columns=[col_name])


df_features = generate_cyclical_features(df_features, 'hour', 24, 0)

# Alternatives to dummy variables. If you want to use this, then you have to remove the
# feature form the one hot encoding feature list

# df_features = generate_cyclical_features(df_features, 'day_of_week', 7, 0)
# df_features = generate_cyclical_features(df_features, 'month', 12, 1)
# df_features = generate_cyclical_features(df_features, 'week_of_year', 52, 0)

print(df_features)

# ### Other date/time-related features
# 
# Considering we are now working with the energy consumption data, one might ask whether holidays
# in a year affect the energy consumption patterns. Indeed, very likely. For such binary
# variables, i.e., 0 or 1, we can generate extra columns with binary values to denote if a given
# date is actually a holiday. Remembering all the holidays or manually defining them is a tedious
# task, to say the least. Fortunately, a package, called holidays, does what it promises to do.

us_holidays = holidays.US()


def is_holiday(date):
    date = date.replace(hour=0)
    return 1 if (date in us_holidays) else 0


df_features = df_features.assign(is_holiday=df.index.to_series().apply(is_holiday))
print(df_features)


# When it comes to feature engineering, possibilities are seemingly limitless and there is
# certainly room for some experimentation and creativity. The good news is there are already
# quite a few packages out there that do the job for us. The bad one is, there is generally no
# clear answer to which additional features may improve the model performance.

# ## Splitting the data into test, validation, and train sets

# After creating feature columns, we split
# the dataset into three different datasets: training, validation, and test sets.


def feature_label_split(df, target_col):
    y = df[[target_col]]
    X = df.drop(columns=[target_col])
    return X, y


def train_val_test_split(df, target_col, test_ratio):
    val_ratio = test_ratio / (1 - test_ratio)
    X, y = feature_label_split(df, target_col)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_ratio, shuffle=False)
    X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=val_ratio,
                                                      shuffle=False)
    return X_train, X_val, X_test, y_train, y_val, y_test


X_train, X_val, X_test, y_train, y_val, y_test = train_val_test_split(df_features, 'value', 0.2)


# ### Applying scale transformation
# 
# Scaling the values in your dataset is a highly recommended practice for neural networks,
# as it is for other machine learning techniques. It speeds up the learning by making it easier
# for the model to update the weights. You can easily do that by using Scikit-learn's scalers,
# MinMaxScaler, RobustScaler, StandardScaler, and the like. For more information on the effects
# of each scaler, please refer to the official documentation.
# 
# And, here is a cool trick if you're looking for a way to switch between scalers quickly. Get
# yourself comfortable with the switcher function; we may use it again later on.

# In[ ]:


def get_scaler(scaler):
    scalers = {
        "minmax": MinMaxScaler,
        "standard": StandardScaler,
        "maxabs": MaxAbsScaler,
        "robust": RobustScaler,
    }
    return scalers.get(scaler.lower())()


scaler = get_scaler('minmax')
X_train_arr = scaler.fit_transform(X_train)
X_val_arr = scaler.transform(X_val)
X_test_arr = scaler.transform(X_test)

y_train_arr = scaler.fit_transform(y_train)
y_val_arr = scaler.transform(y_val)
y_test_arr = scaler.transform(y_test)

# ### Loading the data into DataLoaders
# 
# After you standardize your data, you are usually good to go. Not so fast, this time. After
# spending quite some time working with PyTorch and going through others' code on the internet,
# I noticed most people ended up doing the matrix operations for mini-batch training, i.e.,
# slicing the data into smaller batches, using NumPy. You may think that's what NumPy is for; I
# get it. But there is also a more elegant PyTorch way of doing it, which certainly gets much
# less attention than it should, in my opinion.
# 
# PyTorch's DataLoader class, a Python iterable over Dataset, loads the data and splits them into
# batches for you to do mini-batch training. The most important argument for the DataLoader
# constructor is the Dataset, which indicates a dataset object to load data from. There are
# mainly two types of datasets, one being map-style datasets and the other iterable-style
# datasets.
# 
# In this tutorial, I'll use the latter, but feel free to check them out in [the official
# documentation](https://pytorch.org/docs/stable/data.html). It is also possible to write your
# own Dataset or DataLoader classes for your requirements, but that's definitely beyond the scope
# of this post as the built-in constructors would do more than suffice. But here's a link to the
# official tutorial on the topic.
# 
# For now, I'll be using the class called TensorDataset, a dataset class wrapping the tensors.
# Since Scikit-learn's scalers output NumPy arrays, I need to convert them into Torch tensors to
# load them into TensorDatasets. After creating Tensor datasets for each dataset, I'll use them
# to create my DataLoaders. You may notice an extra DataLoader with the batch size of 1 and
# wonder why the hell we need it. I'll get to that in a bit.

batch_size = 64

train_features = torch.Tensor(X_train_arr)
train_targets = torch.Tensor(y_train_arr)
val_features = torch.Tensor(X_val_arr)
val_targets = torch.Tensor(y_val_arr)
test_features = torch.Tensor(X_test_arr)
test_targets = torch.Tensor(y_test_arr)

train = TensorDataset(train_features, train_targets)
val = TensorDataset(val_features, val_targets)
test = TensorDataset(test_features, test_targets)

train_loader = DataLoader(train, batch_size=batch_size, shuffle=False, drop_last=True)
val_loader = DataLoader(val, batch_size=batch_size, shuffle=False, drop_last=True)
test_loader = DataLoader(test, batch_size=batch_size, shuffle=False, drop_last=True)
test_loader_one = DataLoader(test, batch_size=1, shuffle=False, drop_last=True)


# ## Defining the RNN model classes
# 
# I don't think I can ever do justice to RNNs if I try to explain the nitty-gritty of how they
# work in just a few sentences here. Fortunately, there are several well-written articles on
# these networks for those who are looking for a place to start, Andrej Karpathy's [The
# Unreasonable Effectiveness of Recurrent Neural Networks](
# http://karpathy.github.io/2015/05/21/rnn-effectiveness/), Chris Olah's [Understanding LSTM
# networks](https://colah.github.io/posts/2015-08-Understanding-LSTMs/), and Michael Phi's [
# Illustrated Guide to LSTM's and GRU's: A step by step explanation](
# https://towardsdatascience.com/illustrated-guide-to-lstms-and-gru-s-a-step-by-step-explanation
# -44e9eb85bf21) are a few that come to mind.
# 
# However, traditional neural networks can't do this, and they start from scratch every time they
# are given a task, pretty much like Leonard, you see. RNN addresses this shortcoming. To make a
# gross oversimplification, they do so by looping the information from one step of the network to
# the next, allowing information to persist within the network. This makes them a pretty strong
# candidate to solve various problems involving sequential data, such as speech recognition,
# language translation, or time-series forecasting, as we will see in a bit.
# 

# ### Vanilla RNN By extending PyTorch's nn.Module, a base class for all neural network modules,
# we define our RNN module as follows. Our RNN module will have one or more RNN layers connected
# by a fully connected layer to convert the RNN output into desired output shape. We also need to
# define the forward propagation function as a class method, called forward(). This method is
# executed sequentially, passing the inputs and the zero-initialized hidden state. Nonetheless,
# PyTorch automatically creates and computes the backpropagation function backward().


class RNNModel(nn.Module):
    def __init__(self, input_dim, hidden_dim, layer_dim, output_dim, dropout_prob):
        """The __init__ method that initiates an RNN instance.

        Args:
            input_dim (int): The number of nodes in the input layer
            hidden_dim (int): The number of nodes in each layer
            layer_dim (int): The number of layers in the network
            output_dim (int): The number of nodes in the output layer
            dropout_prob (float): The probability of nodes being dropped out

        """
        super(RNNModel, self).__init__()

        # Defining the number of layers and the nodes in each layer
        self.hidden_dim = hidden_dim
        self.layer_dim = layer_dim

        # RNN layers
        self.rnn = nn.RNN(
            input_dim, hidden_dim, layer_dim, batch_first=True, dropout=dropout_prob
        )
        # Fully connected layer
        self.fc = nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        """The forward method takes input tensor x and does forward propagation

        Args:
            x (torch.Tensor): The input tensor of the shape (batch size, sequence length, input_dim)

        Returns:
            torch.Tensor: The output tensor of the shape (batch size, output_dim)

        """
        # Initializing hidden state for first input with zeros
        h0 = torch.zeros(
            self.layer_dim, x.shape[0], self.hidden_dim).requires_grad_().to(device=device)

        # Forward propagation by passing in the input and hidden state into the model
        out, h0 = self.rnn(x, h0.detach())

        # Reshaping the outputs in the shape of (batch_size, seq_length, hidden_size)
        # so that it can fit into the fully connected layer
        out = out[:, -1, :]

        # Convert the final state to our desired output shape (batch_size, output_dim)
        out = self.fc(out)
        return out


# Vanilla RNN has one shortcoming, though. Simple RNNs can connect previous information to the
# current one, where the temporal gap between the relevant past information and the current one
# is small. As that gap grows, RNNs become less capable of learning the long-term dependencies,
# also known as the vanishing gradient problem. This is where LSTM comes for help.

# ### Long Short-Term Memory (LSTM) Long Short-Term Memory, LSTM for short, is a special type of
# recurrent network capable of learning long-term dependencies and tends to work much better than
# the standard version on a wide variety of tasks. RNNs on steroids, so to speak.
# 
# 
# The standard version's main difference is that, in addition to the hidden state, LSTMs have the
# cell state, which works like a conveyor belt that carries the relevant information from the
# earlier steps to later steps. Along the way, the new information is added to or removed from
# the cell state via input and forget gates, two neural networks that determine which information
# is relevant. From the implementation standpoint, you don't really have to bother with such
# details. All you need to add is a cell state in your forward() method.


class LSTMModel(nn.Module):
    """LSTMModel class extends nn.Module class and works as a constructor for LSTMs.

       LSTMModel class initiates a LSTM module based on PyTorch's nn.Module class.
       It has only two methods, namely init() and forward(). While the init()
       method initiates the model with the given input parameters, the forward()
       method defines how the forward propagation needs to be calculated.
       Since PyTorch automatically defines back propagation, there is no need
       to define back propagation method.

       Attributes:
           hidden_dim (int): The number of nodes in each layer
           layer_dim (str): The number of layers in the network
           lstm (nn.LSTM): The LSTM model constructed with the input parameters.
           fc (nn.Linear): The fully connected layer to convert the final state
                           of LSTMs to our desired output shape.

    """

    def __init__(self, input_dim, hidden_dim, layer_dim, output_dim, dropout_prob):
        """The __init__ method that initiates a LSTM instance.

        Args:
            input_dim (int): The number of nodes in the input layer
            hidden_dim (int): The number of nodes in each layer
            layer_dim (int): The number of layers in the network
            output_dim (int): The number of nodes in the output layer
            dropout_prob (float): The probability of nodes being dropped out

        """
        super(LSTMModel, self).__init__()

        # Defining the number of layers and the nodes in each layer
        self.hidden_dim = hidden_dim
        self.layer_dim = layer_dim

        # LSTM layers
        self.lstm = nn.LSTM(
            input_dim, hidden_dim, layer_dim, batch_first=True, dropout=dropout_prob
        )

        # Fully connected layer
        self.fc = nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        """The forward method takes input tensor x and does forward propagation

        Args:
            x (torch.Tensor): The input tensor of the shape (batch size, sequence length, input_dim)

        Returns:
            torch.Tensor: The output tensor of the shape (batch size, output_dim)

        """
        # Initializing hidden state for first input with zeros, h0 shape torch.Size([3, 64, 64])
        h0 = torch.zeros(
            self.layer_dim, x.shape[0], self.hidden_dim).requires_grad_().to(device=device)

        # Initializing cell state for first input with zeros
        c0 = torch.zeros(self.layer_dim, x.shape[0], self.hidden_dim
                         ).requires_grad_().to(device=device)

        # We need to detach as we are doing truncated backpropagation through time (BPTT) If we
        # don't, we'll backprop all the way to the start even after going through another batch
        # Forward propagation by passing in the input, hidden state, and cell state into the model
        out, (hn, cn) = self.lstm(x, (h0.detach(), c0.detach()))

        # Reshaping the outputs in the shape of (batch_size, seq_length, hidden_size)
        # so that it can fit into the fully connected layer
        out = out[:, -1, :]

        # Convert the final state to our desired output shape (batch_size, output_dim)
        out = self.fc(out)

        return out


# ### Gated Recurrent Unit (GRU) Gated Recurrent Units (GRU) is a slightly more streamlined
# variant that provides comparable performance and considerably faster computation. Like LSTMs,
# they also capture long-term dependencies, but they do so using reset and update gates without
# any cell state. 
# 
# While the update gate determines how much of the past information needs to be kept, the reset
# gate decides how much of the past information to forget. Doing fewer tensor operations,
# GRUs are often faster and require less memory than LSTMs. As you see below, its model class is
# almost identical to the RNN's.

# In[ ]:


class GRUModel(nn.Module):
    """GRUModel class extends nn.Module class and works as a constructor for GRUs.

       GRUModel class initiates a GRU module based on PyTorch's nn.Module class.
       It has only two methods, namely init() and forward(). While the init()
       method initiates the model with the given input parameters, the forward()
       method defines how the forward propagation needs to be calculated.
       Since PyTorch automatically defines back propagation, there is no need
       to define back propagation method.

       Attributes:
           hidden_dim (int): The number of nodes in each layer
           layer_dim (str): The number of layers in the network
           gru (nn.GRU): The GRU model constructed with the input parameters.
           fc (nn.Linear): The fully connected layer to convert the final state
                           of GRUs to our desired output shape.

    """

    def __init__(self, input_dim, hidden_dim, layer_dim, output_dim, dropout_prob):
        """The __init__ method that initiates a GRU instance.

        Args:
            input_dim (int): The number of nodes in the input layer
            hidden_dim (int): The number of nodes in each layer
            layer_dim (int): The number of layers in the network
            output_dim (int): The number of nodes in the output layer
            dropout_prob (float): The probability of nodes being dropped out

        """
        super(GRUModel, self).__init__()

        # Defining the number of layers and the nodes in each layer
        self.layer_dim = layer_dim
        self.hidden_dim = hidden_dim

        # GRU layers
        self.gru = nn.GRU(
            input_dim, hidden_dim, layer_dim, batch_first=True, dropout=dropout_prob
        )

        # Fully connected layer
        self.fc = nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        """The forward method takes input tensor x and does forward propagation

        Args:
            x (torch.Tensor): The input tensor of the shape (batch size, sequence length, input_dim)

        Returns:
            torch.Tensor: The output tensor of the shape (batch size, output_dim)

        """
        # Initializing hidden state for first input with zeros
        h0 = torch.zeros(
            self.layer_dim, x.shape[0], self.hidden_dim).requires_grad_().to(device=device)

        # Forward propagation by passing in the input and hidden state into the model
        out, _ = self.gru(x, h0.detach())

        # Reshaping the outputs in the shape of (batch_size, seq_length, hidden_size)
        # so that it can fit into the fully connected layer
        out = out[:, -1, :]

        # Convert the final state to our desired output shape (batch_size, output_dim)
        out = self.fc(out)

        return out


# Similar to the trick we do with scalers, we can also easily switch between these models we just
# created.


def get_model(model, model_params) -> nn.Module:
    models = {
        "rnn": RNNModel,
        "lstm": LSTMModel,
        "gru": GRUModel,
    }
    return models.get(model.lower())(**model_params)


# Now, it seems like we got everything ready to train our RNN models. But where do we start?

# ## Making predictions
# 
# Let's start by creating the main framework for training the models. There are probably heaps of
# ways to do this, and one of them is to use a helper, or a wrapper, class that holds the
# training, validation, and evaluation methods. First, we need to have a model class,
# a loss function to calculate the losses, and an optimizer to update the weights in the network.
# 
# ### Helper/Wrapper Class for training
# 
# If you're familiar with neural networks, you already know that training them is a rather
# repetitive process, looping back and forth between forward-prop and back-prop. I find it useful
# to have one level of abstraction, a train step function or wrapper, to combine these repetitive
# steps.
# 
# After defining one proper training step, we can now move onto writing the training loop where
# this step function will be called at each epoch. During each epoch in training, there are two
# stages: training and validation. After each training step, the network's weights are tweaked a
# bit to minimize the loss function. Then, the validation step will evaluate the current state of
# the model to see if there has been any improvement after the most recent update. 
# 
# As I'll be using mini-batch training, a training technique where only a portion of data is used
# at each epoch, there will be two for loops for each stage where a model is trained and
# validated batch by batch. This usually requires reshaping each batch tensor into the correct
# input dimensions so that the network can use it as an input.
# 
# Another important thing to note is to activate the train() mode during training and the eval()
# mode during the validation. While the train() mode allows the network's weights to be updated,
# the evaluation () mode signals the model that there is no need to calculate the gradients.
# Hence, the weights stay the same.
# 
# Now, we can finally train our model. However, without evaluating these models with a separate
# test set, i.e., a hold-out set, it would be impossible to tell how the model performs than
# other models we're building. Much similar to the validation loop in the train() method,
# we'll define a testing method to evaluate our models as follows.
# 
# During the training, the loss function outputs are generally a good indicator of whether the
# model is learning, overfitting, or underfitting. For this reason, we'll be plotting simple loss
# figures by using the following method.


class Optimization:
    """Optimization is a helper class that allows training, validation, prediction.

    Optimization is a helper class that takes model, loss function, optimizer function
    learning scheduler (optional), early stopping (optional) as inputs. In return, it
    provides a framework to train and validate the models, and to predict future values
    based on the models.

    Attributes:
        model (RNNModel, LSTMModel, GRUModel): Model class created for the type of RNN
        loss_fn (torch.nn.modules.Loss): Loss function to calculate the losses
        optimizer (torch.optim.Optimizer): Optimizer function to optimize the loss function
        train_losses (list[float]): The loss values from the training
        val_losses (list[float]): The loss values from the validation
    """

    def __init__(self, model, loss_fn, optimizer):
        """
        Args:
            model (RNNModel, LSTMModel, GRUModel): Model class created for the type of RNN
            loss_fn (torch.nn.modules.Loss): Loss function to calculate the losses
            optimizer (torch.optim.Optimizer): Optimizer function to optimize the loss function
        """
        self.model = model
        self.loss_fn = loss_fn
        self.optimizer = optimizer
        self.train_losses = []
        self.val_losses = []

    def train_step(self, x, y):
        """The method train_step completes one step of training.

        Given the features (x) and the target values (y) tensors, the method completes
        one step of the training. First, it activates the train mode to enable back prop.
        After generating predicted values (yhat) by doing forward propagation, it calculates
        the losses by using the loss function. Then, it computes the gradients by doing
        back propagation and updates the weights by calling step() function.

        Args:
            x (torch.Tensor): Tensor for features to train one step
            y (torch.Tensor): Tensor for target values to calculate losses

        """
        # Sets model to train mode
        self.model.train()

        # Makes predictions
        yhat = self.model(x)

        # Computes loss
        loss = self.loss_fn(y, yhat)

        # Computes gradients
        loss.backward()

        # Updates parameters and zeroes gradients
        self.optimizer.step()
        self.optimizer.zero_grad()

        # Returns the loss
        return loss.item()

    def train(self, train_loader, val_loader, batch_size=64, n_epochs=50, n_features=1):
        """The method train performs the model training

        The method takes DataLoaders for training and validation datasets, batch size for
        mini-batch training, number of epochs to train, and number of features as inputs.
        Then, it carries out the training by iteratively calling the method train_step for
        n_epochs times. If early stopping is enabled, then it  checks the stopping condition
        to decide whether the training needs to halt before n_epochs steps. Finally, it saves
        the model in a designated file path.

        Args:
            train_loader (torch.utils.data.DataLoader): DataLoader that stores training data
            val_loader (torch.utils.data.DataLoader): DataLoader that stores validation data
            batch_size (int): Batch size for mini-batch training
            n_epochs (int): Number of epochs, i.e., train steps, to train
            n_features (int): Number of feature columns

        """
        model_path = f'{self.model}_{datetime.now().strftime("%Y-%m-%d %H:%M:%S")}'

        for epoch in range(1, n_epochs + 1):
            batch_losses = []
            for x_batch, y_batch in train_loader:
                x_batch = x_batch.view([batch_size, -1, n_features]).to(device)
                y_batch = y_batch.to(device)
                loss = self.train_step(x_batch, y_batch)
                batch_losses.append(loss)
            training_loss = np.mean(batch_losses)
            self.train_losses.append(training_loss)

            with torch.no_grad():
                batch_val_losses = []
                for x_val, y_val in val_loader:
                    x_val = x_val.view([batch_size, -1, n_features]).to(device)
                    y_val = y_val.to(device)
                    self.model.eval()
                    yhat = self.model(x_val)
                    val_loss = self.loss_fn(y_val, yhat).item()
                    batch_val_losses.append(val_loss)
                validation_loss = np.mean(batch_val_losses)
                self.val_losses.append(validation_loss)

            if (epoch <= 10) | (epoch % 50 == 0):
                print(
                    f"[{epoch}/{n_epochs}] "
                    f"Training loss: {training_loss:.4f}\t "
                    f"Validation loss: {validation_loss:.4f} "
                )

        torch.save(self.model.state_dict(), model_path)

    def evaluate(self, test_loader, batch_size=1, n_features=1):
        """The method evaluate performs the model evaluation

        The method takes DataLoaders for the test dataset, batch size for mini-batch testing,
        and number of features as inputs. Similar to the model validation, it iteratively
        predicts the target values and calculates losses. Then, it returns two lists that
        hold the predictions and the actual values.

        Note:
            This method assumes that the prediction from the previous step is available at
            the time of the prediction, and only does one-step prediction into the future.

        Args:
            test_loader (torch.utils.data.DataLoader): DataLoader that stores test data
            batch_size (int): Batch size for mini-batch training
            n_features (int): Number of feature columns

        Returns:
            list[float]: The values predicted by the model
            list[float]: The actual values in the test set.

        """
        with torch.no_grad():
            predictions = []
            values = []
            for x_test, y_test in test_loader:
                x_test = x_test.view([batch_size, -1, n_features]).to(device)
                y_test = y_test.to(device)
                self.model.eval()
                yhat = self.model(x_test)
                predictions.append(yhat.cpu().detach().numpy())
                values.append(y_test.cpu().detach().numpy())

        return predictions, values

    def plot_losses(self):
        """The method plots the calculated loss values for training and validation
        """
        plt.plot(self.train_losses, label="Training loss")
        plt.plot(self.val_losses, label="Validation loss")
        plt.legend()
        plt.title("Losses")
        plt.show()
        plt.close()


# ### Training the model
# 
# So far, we have prepared our dataset, defined our model classes and the wrapper class. We need
# to put all of them together. Without further ado, let's start training our model.
#

input_dim = len(X_train.columns)
output_dim = 1
hidden_dim = 64
layer_dim = 3
batch_size = 64
dropout = 0.2
n_epochs = 50
learning_rate = 1e-3
weight_decay = 1e-6

model_params = {'input_dim': input_dim,
                'hidden_dim': hidden_dim,
                'layer_dim': layer_dim,
                'output_dim': output_dim,
                'dropout_prob': dropout}

model = get_model('lstm', model_params).to(device=device)

loss_fn = nn.MSELoss(reduction="mean")
optimizer = optim.Adam(model.parameters(), lr=learning_rate, weight_decay=weight_decay)

opt = Optimization(model=model, loss_fn=loss_fn, optimizer=optimizer)
opt.train(train_loader, val_loader, batch_size=batch_size, n_epochs=n_epochs, n_features=input_dim)
opt.plot_losses()

predictions, values = opt.evaluate(
    test_loader_one,
    batch_size=1,
    n_features=input_dim
)


# ### Formatting the predictions
# 
# As you may recall, we trained our network with standardized inputs; therefore, all the model's
# predictions are also scaled. Also, after using batching in our evaluation method, all of our
# predictions are now in batches. To calculate error metrics and plot these predictions,
# we need first to reduce these multi-dimensional tensors to a one-dimensional vector, i.e.,
# flatten, and then apply inverse_transform() to get the predictions' real values.


def inverse_transform(scaler, df, columns):
    for col in columns:
        df[col] = scaler.inverse_transform(df[col])
    return df


def format_predictions(predictions, values, df_test, scaler):
    vals = np.concatenate(values, axis=0).ravel()
    preds = np.concatenate(predictions, axis=0).ravel()
    df_result = pd.DataFrame(data={"value": vals, "prediction": preds},
                             index=df_test.head(len(vals)).index)
    df_result = df_result.sort_index()
    df_result = inverse_transform(scaler, df_result, [["value", "prediction"]])
    return df_result


df_result = format_predictions(predictions, values, X_test, scaler)
print(df_result)


# ### Calculating error metrics
# 
# After flattening and de-scaling the values, we can now calculate error metrics, such as mean
# absolute error (MAE), mean squared error (MSE), and root mean squared error (RMSE).


def calculate_metrics(df):
    result_metrics = {'mae': mean_absolute_error(df.value, df.prediction),
                      'rmse': mean_squared_error(df.value, df.prediction) ** 0.5,
                      'r2': r2_score(df.value, df.prediction)}

    print("Mean Absolute Error:       ", result_metrics["mae"])
    print("Root Mean Squared Error:   ", result_metrics["rmse"])
    print("R^2 Score:                 ", result_metrics["r2"])
    return result_metrics


result_metrics = calculate_metrics(df_result)


# ### Generating baseline predictions
# 
# Having some sort of baseline model helps us compare how our models actually do at prediction.
# For this task, I've chosen good old linear regression, good enough to generate reasonable
# baseline but simple enough to do it very fast.


def build_baseline_model(X_train, y_train):
    model = LinearRegression()
    model.fit(X_train, y_train)
    return model


def baseline_model_predict(model, X, y):
    prediction = model.predict(X)
    result = pd.DataFrame(y)
    result["prediction"] = prediction
    result = result.sort_index()
    return result


X, y = feature_label_split(df_features, 'value')

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, shuffle=False
)

baseline_model = build_baseline_model(X_train, y_train)
df_baseline = baseline_model_predict(baseline_model, X_test, y_test)
baseline_metrics = calculate_metrics(df_baseline)


# ### Visualizing the predictions
# 
# Last but not least, visualizing your results helps you better understand how your model
# performs and adds what kind of features would likely improve it. I'll be using Plotly again,
# but feel free to use a package that you are more comfortable with.


def plot_predictions(df_result, df_baseline):
    data = []

    value = go.Scatter(
        x=df_result.index,
        y=df_result.value,
        mode="lines",
        name="values",
        marker=dict(),
        text=df_result.index,
        line=dict(color="rgba(0,0,0, 0.3)"),
    )
    data.append(value)

    baseline = go.Scatter(
        x=df_baseline.index,
        y=df_baseline.prediction,
        mode="lines",
        line={"dash": "dot"},
        name='linear regression',
        marker=dict(),
        text=df_baseline.index,
        opacity=0.8,
    )
    data.append(baseline)

    prediction = go.Scatter(
        x=df_result.index,
        y=df_result.prediction,
        mode="lines",
        line={"dash": "dot"},
        name='predictions',
        marker=dict(),
        text=df_result.index,
        opacity=0.8,
    )
    data.append(prediction)

    layout = dict(
        title="Predictions vs Actual Values for the dataset",
        xaxis=dict(title="Time", ticklen=5, zeroline=False),
        yaxis=dict(title="Value", ticklen=5, zeroline=False),
    )

    fig = dict(data=data, layout=layout)
    iplot(fig)


# Set notebook mode to work in offline
# pyo.init_notebook_mode()

plot_predictions(df_result, df_baseline)

# In sample predictions

predictions, values = opt.evaluate(
    train_loader,
    batch_size=64,
    n_features=input_dim
)

df_result = format_predictions(predictions, values, X_train, scaler)
print(df_result)

df_baseline = baseline_model_predict(baseline_model, X_train, y_train)
plot_predictions(df_result, df_baseline)
