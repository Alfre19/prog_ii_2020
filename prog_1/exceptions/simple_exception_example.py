from math import log2
try:
    x = log2('a')
    print(x)
except ValueError:
    print('log domain error')
except TypeError:
    print('log type error')
