class User:
    user_dic = dict()

    def __init__(self, user_id: str, name: str):
        self.user_id = user_id
        self.name = name

    @classmethod
    def new_user(cls, user_id: str, name: str):
        user = cls(user_id, name)
        User.user_dic.update({user_id: user})
        return user

    @classmethod
    def get_user(cls, user_id: str):
        return cls.user_dic.get(user_id)


user_1 = User.new_user('123', 'Bob')
user_2 = User.new_user('456', 'Frank')
print(User.user_dic)
User.get_user('123')
