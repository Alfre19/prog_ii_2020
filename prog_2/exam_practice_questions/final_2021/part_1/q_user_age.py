from dataclasses import dataclass


@dataclass
class ShppingCart:
    total: float
    items: dict[str, int]

    def __add__(self, other: 'ShppingCart') -> 'ShppingCart':
        return ShppingCart(
            total=self.total + other.total,
            items=dict(**self.items, **other.items)
        )


a = ShppingCart(100.0, {'S100': 1, 'S101': 1})
b = ShppingCart(200.0, {'S200': 2, 'S201': 3})

print(a + b)


@dataclass
class ShoppingCartAdv:
    total: float
    items: dict[str, int]

    def __add__(self, other: 'ShoppingCartAdv') -> 'ShoppingCartAdv':

        skus = set(self.items.keys())
        skus =  skus.union(set(other.items.keys()))

        totals = {sku: self.items.get(sku, 0) + other.items.get(sku, 0) for sku in skus}

        return ShoppingCartAdv(
            total=self.total + other.total,
            items=totals
        )


a = ShoppingCartAdv(100.0, {'S100': 1, 'S101': 1})
b = ShoppingCartAdv(200.0, {'S100': 2, 'S201': 3})

print(a + b)
