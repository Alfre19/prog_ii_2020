import itertools

# Drop whicle predicate is true
values = [('A', 1), ('B', 0), ('D', 4), ('F', 7)]
r3 = list(itertools.dropwhile(lambda x: x[1] < 2, values))
print(r3)

# Create iterator with elements from supplied iterator between
#  the two indexes (use ‘None’ for second index to go to end)
r4 = list(itertools.islice(values, 3, None))
print(r4)
