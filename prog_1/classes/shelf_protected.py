from dataclasses import dataclass


@dataclass
class Publication:
    title: str
    author: str


@dataclass
class Book(Publication):
    chapters: list[str]


@dataclass
class Magazine(Publication):
    image_list: list[str]


@dataclass
class Bookshelf:
    def __post_init__(self):
        self._shelf = {}


