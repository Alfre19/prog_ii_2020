from matplotlib import pyplot as plt
import torch
from torch import nn, optim
from torchvision import datasets, transforms

torch.set_printoptions(edgeitems=2, linewidth=75)
torch.manual_seed(123)

data_path = 'ml/data-unversioned/'

class_names = ['airplane', 'automobile', 'bird', 'cat', 'deer',
               'dog', 'frog', 'horse', 'ship', 'truck']

label_map = {0: 0, 2: 1, 1: 2}

cifar10 = datasets.CIFAR10(
    data_path, train=True, download=False,
    transform=transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.4915, 0.4823, 0.4468),
                             (0.2470, 0.2435, 0.2616))
    ]),
    target_transform=lambda label: label_map[label] if label in label_map else label
)

cifar10_val = datasets.CIFAR10(
    data_path,
    train=False,
    download=False,
    transform=transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.4915, 0.4823, 0.4468),
                             (0.2470, 0.2435, 0.2616))
    ]),
    target_transform=lambda label: label_map[label] if label in label_map else label
)

birds_aeroplanes_train = [index for index, sample in enumerate(cifar10) if sample[1] in {0, 1}]
birds_aeroplanes_val = [index for index, sample in enumerate(cifar10_val) if sample[1] in {0, 1}]

cifar2 = torch.utils.data.Subset(cifar10, birds_aeroplanes_train)
cifar2_val = torch.utils.data.Subset(cifar10_val, birds_aeroplanes_val)

len(cifar2)
img_t, label = cifar2[99]

print(label)
plt.imshow(img_t.permute(1, 2, 0))
plt.show()

len(cifar2)
img_t, label = cifar2[100]

print(label)
plt.imshow(img_t.permute(1, 2, 0))
plt.show()

train_loader = torch.utils.data.DataLoader(cifar2,
                                           batch_size=64,
                                           shuffle=True)

model = nn.Sequential(
    nn.Linear(3072, 512),
    nn.Tanh(),
    nn.Linear(512, 2))

learning_rate = 1e-2

optimizer = optim.SGD(model.parameters(), lr=learning_rate)

loss_fn = nn.CrossEntropyLoss()

n_epochs = 100

for epoch in range(n_epochs):
    for imgs, labels in train_loader:
        outputs = model(imgs.view(imgs.shape[0], -1))
        loss = loss_fn(outputs, labels)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    print("Epoch: %d, Loss: %f" % (epoch, float(loss)))

correct = 0
total = 0

with torch.no_grad():
    for imgs, labels in train_loader:
        outputs = model(imgs.view(imgs.shape[0], -1))
        _, predicted = torch.max(outputs, dim=1)
        total += labels.shape[0]
        correct += int((predicted == labels).sum())

print("Accuracy: %f" % (correct / total))

val_loader = torch.utils.data.DataLoader(cifar2_val,
                                         batch_size=64,
                                         shuffle=False)

correct = 0
total = 0

with torch.no_grad():
    for imgs, labels in val_loader:
        outputs = model(imgs.view(imgs.shape[0], -1))
        _, predicted = torch.max(outputs, dim=1)
        total += labels.shape[0]
        correct += int((predicted == labels).sum())

print("Accuracy: %f" % (correct / total))

torch.save(model, 'ml/feed_forward/itsabird.pt')