import torch
from torch import nn
from collections import OrderedDict

seq_model_unnamed = nn.Sequential(
    nn.Linear(1, 13),
    nn.Tanh(),
    nn.Linear(13, 1))

vector_in = torch.ones(1, 1)

o1 = nn.Linear(1, 13).forward(vector_in)
o2 = nn.Tanh().forward(o1)
o3 = nn.Linear(13, 1).forward(o2)

seq_model_unnamed.forward(vector_in)

seq_model = nn.Sequential(OrderedDict([
    ('hidden_linear', nn.Linear(1, 8)),
    ('hidden_activation', nn.Tanh()),
    ('output_linear', nn.Linear(8, 1))
]))

list(seq_model.parameters())