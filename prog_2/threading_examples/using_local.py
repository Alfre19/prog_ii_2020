from threading import Thread, local
from time import sleep

working_list = []


def worker(local_data: local, w_list: list):
    local_data.message = '\nbar'
    for i in range(0, 5):
        print('.', end='', flush=True)
        sleep(0.2)
    w_list.append('foo')
    print(local_data.message)


print('Starting')
data = local()
t = Thread(target=worker, args=(data, working_list))
t.start()
# t.join()
try:
    print(local.message)
except AttributeError:
    print('message not set')

print(working_list)
print('\nDone')
