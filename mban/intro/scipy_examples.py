import numpy as np

v = np.array([1.0, 2.0, 6.0], dtype=np.float32)

x = np.array([[ 0,  1,  2],
              [ 3,  4,  5],
              [ 6,  7,  8],
              [ 9, 10, 11]])

x[[0, 0, 3, 3], [0, 2, 0, 2]]

x > 3