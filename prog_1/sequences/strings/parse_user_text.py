from dataclasses import dataclass
import re


@dataclass
class Location:
    street_name: str
    street_number: int


@dataclass
class Contact:
    name: str
    surname: str
    phone: int
    location: Location

    def get_full_name(self) -> str:
        """
        Returns the full name of a contact
        :return: the full name
        """
        return self.name + ' ' + self.surname

    @classmethod
    def from_record(cls, line: str) -> 'Contact':
        m = re.fullmatch(r'([\w ]+) (\w+): (\d{3}\.\d{3}\.\d{4}) (\d+) (.*)', line)
        phone_str = m.group(3)
        phone = int(re.sub(r'../../../../../../AppData/Roaming/JetBrains/PyCharm2021.3/scratches', '', phone_str))
        location = Location(m.group(5), m.group(4))
        return cls(m.group(1), m.group(2), phone, location)


def read_contacts(text: str) -> dict[str, Contact]:
    d = {}
    for r in re.split(r'\n', text):
        c = Contact.from_record(r)
        d[c.get_full_name()]= c
    return d

# line_1 = 'Ross James McFluff: 834.345.1254 155 Elm Street'
# con = Contact.from_record(line_1)
# loc = Location("Elm Street", 155)
# con = Contact("Ross", "McFluff", 8343451254, loc)
# print(con.get_full_name())


users_text = """Ross McFluff: 834.345.1254 155 Elm Street
Ronald Heathmore: 892.345.3428 436 Finley Avenue
Frank Burger: 925.541.7625 662 South Dogwood Way
Heather Albrecht: 548.326.4584 919 Park Place"""

phone_book = read_contacts(users_text)

print(phone_book['Ross McFluff'])

