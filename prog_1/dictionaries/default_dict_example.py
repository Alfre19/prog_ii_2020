from collections import defaultdict

s = [('yellow', 1), ('blue', 2), ('yellow', 3), ('blue', 4), ('red', 1)]
d = defaultdict(list)
for k, v in s:
    d[k].append(v)

sorted(d.items())

s = 'mississippi'
d = defaultdict(int)
for k in s:
    d[k] += 1

sorted(d.items())

s = 'mississippi'
d = defaultdict(lambda: 0)
for k in s:
    d[k] += 1

sorted(d.items())

# Order of insertion

d = {"one": 1, "two": 2, "three": 3, "four": 4}

it = d.popitem()

d.update({"one": 3, "five": 5})

print(d)


s = [('yellow', 1), ('blue', 2), ('red', 1)]
d = dict(s)
d['red'] = 4
d['black'] = 1

a = d.popitem()
a = d.pop('red')

d = {'john': '789'}
d['peter'] = '987'
d['peter']

d.keys()

names = []
for k in d.keys():
    names.append(k.upper())


s = 2 + 3
f'add {s}'

for k, v in d.items():
    print(k + ',telephone:' + v)

a = d.copy()

d['liza'] = '666'

del d['liza']

n = d.pop('liza')
n = d.popitem()

c = a | d