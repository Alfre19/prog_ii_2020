from itertools import accumulate
from functools import reduce

cashflows = [1000, -90, -90, -90, -90]
list(accumulate(cashflows, lambda bal, pmt: bal*1.05 + pmt))

reduce(lambda bal, pmt: bal*1.05 + pmt, cashflows)

