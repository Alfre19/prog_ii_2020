# This is a typical use of nonlocal, where an enclosed function (g) modifies
# a variable defined in the enclosing namespace (of f)

def f():
    counter = 0

    def g():
        nonlocal counter
        counter += 1
        print(counter)

    g()
    g()


f()

"""
T: The counter is updated twice within the enclosing namespace
F: We can accesss counter in the global namespace. For example we can print(counter) after the call
to f.
F: Global should be used instead of nonlocal
T: counter += 1 modifies counter in f
"""