@app.route('/<int:post_id>')
def show_post(post_id):
    post = get_post(post_id)
    return render_template('post.html', post=post)
