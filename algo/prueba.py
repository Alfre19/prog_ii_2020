import numpy as np
from scipy.optimize import linprog


profit = np.array([-350, -300])
"""
linprog optimiza hacia el mínimo, por lo que buscamos el beneficio en negativo
"""

A_ub = np.array([[1, 1], [18, 12], [6, 8]])
b_ub = np.array([200, 3132, 1440])
"""Restricciones"""

A_bound = (0, None)
b_bound = (0, None)
bounds = [A_bound, b_bound]
"""Límites [0, infinito)"""

result = linprog(profit, A_ub=A_ub, b_ub=b_ub, bounds=bounds)
print(result)
"""El resultado es [121.99999239,  77.99999539]"""