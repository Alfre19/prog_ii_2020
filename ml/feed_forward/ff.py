import torch
from torch import nn

linear = nn.Linear(1, 13)
tanh = nn.Tanh()
linear2 = nn.Linear(13, 1)

i = torch.tensor([[15.], [16.]])

o1 = linear.forward(i)
o2 = tanh.forward(o1)
o3 = linear2.forward(o2)

seq_model = nn.Sequential(
    nn.Linear(1, 13),
    nn.Sigmoid(),
    nn.Linear(13, 5),
    nn.Sigmoid(),
    nn.Linear(5, 1)
)

[list(layer.parameters()) for layer in seq_model]