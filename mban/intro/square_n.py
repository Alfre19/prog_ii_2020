import sys


def f(x):
    # This function squares
    return x ** 2


def g(y):
    return y ** 3


if __name__ == '__main__':
    sq = f(int(sys.argv[1]))
    print('The square of ' + sys.argv[1] + ' is ' + str(sq))
