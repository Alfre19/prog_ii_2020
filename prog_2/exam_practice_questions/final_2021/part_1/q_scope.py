CONSTANT = 5
counter = 0


def f():
    def g():
        global counter
        counter += 1

    g()
    g()


print(counter)
f()
print(counter)

"""
T: The counter in g has the same identity as counter defined in the global namespace
F: El uso, en este caso, de nonlocal en la línea 7 sería incorrecto porque counter no está definida en el f.
F: Al ejecutarse primer print counter en el programa principal, g ya se ha llamado dos veces, por lo que counter se ha actualizado dos veces 
F: Al ejecutarse el segundo counter, g ya se ha llamado dos veces, por lo que counter se ha actualizado dos veces 
"""

