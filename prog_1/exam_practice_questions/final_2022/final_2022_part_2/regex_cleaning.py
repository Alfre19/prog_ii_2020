import re

text = """Faculty of Law, Business and Government - UFV Madrid video
¿Conocéis el grado en #Criminologia en la Universidad Francisco de Vitoria? La directora del grado, 
ESTER PASCUAL RODRÍGUEZ, el alumno de 4º, Jaime Antonio Malaxechevarría Mérida, y Miriam e 
Iria Alonso alumnas de 3º, nos explican de qué trata. #FDEG #grado #master #formacionejecutiva"""

re.findall(r'#\w+', text)
# ['#Criminologia', '#FDEG', '#grado', '#master', '#formacionejecutiva']

text2 = """
Ayer pudimos disfrutar de un encuentro entre Felipe Calderón, presidente de México de 2006 a 2012, 
y José María Aznar, expresidente de Gobierno y presidente del IADG, en una nueva sesión del Aula 
de Liderazgo Instituto Atlántico de Gobierno IADG-Universidad Francisco de Vitoria, que fue 
moderada Javier Redondo, profesor y articulista. #IADG #MasterAccionPoliticaUFV #FDEGUFV"""


m_iter = re.finditer(r'(([A-Z]\w+ )+[A-Z]\w+), (ex)?presidente', text2)
[m.group(1) for m in m_iter]
"""
Felipe Calderón
José María Aznar
"""

m_iter = re.finditer(r'(([A-Z]\w+ )+[A-Z]\w+),', text2)
for m in m_iter:
    print(m.group(1))

"""
Felipe Calderón
José María Aznar
Javier Redondo
"""