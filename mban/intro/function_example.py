import math


def f(x):
    y = math.log(x)
    return y


for i in [2, 3, 4]:
    t = f(i)
    print(t)
