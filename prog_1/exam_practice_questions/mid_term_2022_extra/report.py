import datetime as dt


class Report:
    def __init__(self, client_id: str, note: str):
        """
        Report class, baseline
        :param client_id: ID of the client
        :param note: note of the report
        """
        self.last_access = dt.datetime.now()
        self.client_id = client_id
        self.note = note

    def print_report(self):
        """Print report with last acesss"""
        self.last_access = dt.datetime.now()
        print(f'Report for client id: {self.client_id}\nNote: '
              f'{self.note}\nLast access: {self.last_access}')


class MaintenanceReport(Report):

    def __init__(self, client_id: str, note: str, parts: list[str]):
        """
        A class representing a maintenance report
        :param client_id: ID of the client
        :param note: note of the report
        :param parts: list of spare parts
        """
        super().__init__(client_id, note)
        self.parts = []
        for part in parts:
            if isinstance(part, str):
                self.parts.append(part)
            else:
                raise TypeError

    def print_report(self):
        """Print report with last acesss and parts"""
        self.last_access = dt.datetime.now()
        print(f'Report for client id: {self.client_id}\nNote: {self.note}\nLast access: '
              f'{self.last_access}\n Parts: {self.parts}')


report = MaintenanceReport('3332', '25000 km vehicle maintenance requested', ['A1', 'A2'])
report.print_report()

try:
    report = MaintenanceReport('3332', '25000 km vehicle maintenance requested', ['A1', 3])
except TypeError:
    print('Error en la definición de la partes')