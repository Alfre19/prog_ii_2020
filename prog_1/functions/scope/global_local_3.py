# This does not generate an error, but, again, local_var in iner is shadowing the same name
# in an outer scope
# Inner does not change outer's local_var

def outer():
    def inner():
        local_var = 7
        print(local_var)

    local_var = 2
    print(local_var)
    inner()
    print(local_var)

outer()